FROM digitallyseamless/nodejs-bower-grunt
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY package.json /usr/src/app/
RUN npm install
COPY bower.json .bowerrc* /usr/src/app/
RUN bower install
COPY . /usr/src/app
EXPOSE 8082
CMD [ "npm", "start"]
