/*******************************************************************************
*   © 2017 UniFyd.  All Rights Reserved.
*
*   CONFIDENTIAL BUSINESS INFORMATION
*   
*   This software includes proprietary, trade secret and confidential
*   information of UniFyd Organization and its licensors.  Use of 
*   this software is strictly subject to and governed by the terms
*   and conditions of a written software license agreement between
*   UniFyd Organization or its legal predecessor, and the
*   institution to which such license for use has been granted.
********************************************************************************/
'use strict';
var mongoose = require('mongoose'),
    logger   = require('./logger.server.config');

mongoose.Promise = require('request-promise');

///////////////////////////////////////////////////////////////////////////
//  Start Mongoose and connect to Mongodb
///////////////////////////////////////////////////////////////////////////
module.exports = function() {
    var mongoUrl = "mongodb://172.16.25.61:27017/unifyd";
    mongoose.connect(
      mongoUrl,
      function(err) {
        if (err) {
          logger.error(__filename,
            " - Critical error: Couldn't start mongodb instance:"
          );
          logger.error(err);
          return;
        }
        logger.info("Connected to mongodb instance");
      });
    var db = mongoose.connection;
    db.on('error', function callback(err) {
      logger.error(__filename, " - Database connection failed. Error: " + err);
    });
    db.once('open', function callback() {
      logger.info(__filename, " - Database connection successful.");
    });
    return db;
  }
  //////////////////////////////////////////////////////////////////////////
