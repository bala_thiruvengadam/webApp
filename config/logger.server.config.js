/*******************************************************************************
*   © 2017 UniFyd.  All Rights Reserved.
*
*   CONFIDENTIAL BUSINESS INFORMATION
*   
*   This software includes proprietary, trade secret and confidential
*   information of UniFyd Organization and its licensors.  Use of 
*   this software is strictly subject to and governed by the terms
*   and conditions of a written software license agreement between
*   UniFyd Organization or its legal predecessor, and the
*   institution to which such license for use has been granted.
********************************************************************************/
const winston = require('winston');
const logDir = 'log';

winston.emitErrs = true;


var logger = new winston.Logger({
  transports: [
    new winston.transports.Console({
      level: 'info',
      handleExceptions: false,
      json: false,
      timestamp: function() {
        return new Date();
      },
      formatter: function(options) {
        // Return string will be passed to logger.
        return options.timestamp() + ' ' + options.level.toUpperCase() + ': ' +
          (options.message ? options.message : '') +
          (options.meta && Object.keys(options.meta).length ? '\n\t' + JSON.stringify(
            options.meta) : '');
      },
      colorize: true
    })
  ],
  exitOnError: false
});



module.exports = logger;

module.exports.stream = {
  write: function(message, encoding) {
    logger.info(message);
  }
}
