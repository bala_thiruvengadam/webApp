/*******************************************************************************
 *   © 2017 UniFyd.  All Rights Reserved.
 *
 *   CONFIDENTIAL BUSINESS INFORMATION
 *
 *   This software includes proprietary, trade secret and confidential
 *   information of UniFyd Organization and its licensors.  Use of
 *   this software is strictly subject to and governed by the terms
 *   and conditions of a written software license agreement between
 *   UniFyd Organization or its legal predecessor, and the
 *   institution to which such license for use has been granted.
 ********************************************************************************/
var express = require('express');
var app = express();
var mongoose = require('mongoose');
var bodyParser = require('body-parser');
var methodOverride = require('method-override');
var Passport = require('passport').Passport;
var Passports = require("passports");
var passports = new Passports();
var passport = require('passport');
var rp = require('request-promise');
var logger = require('./config/logger.server.config.js');

var domanmapping = {};


const OAuth2Strategy = require('passport-oauth').OAuth2Strategy;

passports._getConfig = function _getConfig(req, cb) {
    console.log("Get config called");
    console.log("Hostname: " + req.hostname);
    console.log("Host: " + req.hostname);
    /*return cb(null, req.hostname, {
        authorizationURL: 'https://idp-preprod.quicklaunchsso.com/oauth2/authorize',
        tokenURL: 'https://idp-preprod.quicklaunchsso.com/oauth2/token',
        clientID: 'BbeVUjWnEz3m0fKbSpsFo_NVB2oa',
        clientSecret: 'Ab0csmwA0YX1U0C1Us4ycim1oIca',
        callbackURL: "http://carbon.super:8082/auth/unifyd/passport/callback",
        userProfileURL: 'https://idp-preprod.quicklaunchsso.com/oauth2/userinfo',
        scope: 'openid',
        scopes: ['openid'],
        tenant: 'CEAI',
        tenantdomain: 'carbon.super'
    });*/

    return cb(null, req.hostname, {
        authorizationURL: 'https://idp.quicklaunchsso.com/oauth2/authorize',
        tokenURL: 'https://idp.quicklaunchsso.com/oauth2/token',
        clientID: 'eL67BdNXW9jlQRVa9VXW9iGtRTka',
        clientSecret: 'cS5udbahxbp0HHlBU2lo8UnLFaQa',
        callbackURL: "http://172.16.25.61:8082/auth/unifyd/passport/callback",
        userProfileURL: 'https://idp.quicklaunchsso.com/oauth2/userinfo',
        scope: 'openid',
        scopes: ['openid'],
        tenant: 'CEAI',
        tenantdomain: 'unifyd.edu'
    });
};

passports._createInstance = function _createInstance(options, cb) {
    var instance = new Passport();


    instance.use('oauth2', new OAuth2Strategy(options,
        function (accessToken, refreshToken, profile, cb) {

            var opt = {
                url: options.userProfileURL,
                headers: {
                    'Authorization': 'Bearer ' + accessToken
                }
            };
            rp(opt)
                .then(function (parsedBody) {
                    // POST succeeded...
                    var userinfo = JSON.parse(parsedBody);
                    console.log(userinfo.sub);
                    console.log("Access token : " + accessToken);
                    console.log("Refresh token : " + refreshToken);
                    console.log("Profile : " + JSON.stringify(profile));
                    console.log("Options tenant :" + options.tenant);
                    var dt = {
                        firstName: '',
                        lastName: '',
                        displayName: profile.displayName,
                        email: '',
                        username: userinfo.sub,
                        provider: profile.provider,
                        providerIdentifierField: 'id',
                        accessToken: accessToken,
                        refreshToken: refreshToken,
                        providerData: accessToken,
                        tenant: options.tenant,
                        tenantdomain: options.tenantdomain
                    };

                    console.log(parsedBody);
                    return cb(null, dt);
                })
                .catch(function (err) {
                    // POST failed...
                });
        }
    ));


    instance.serializeUser(function (user, cb) {
        //user.realm = options.realm;

        cb(null, JSON.stringify(user));
    });

    instance.deserializeUser(function (id, cb) {
        cb(null, JSON.parse(id));
    });

    cb(null, instance);
};

process.env.NODE_TLS_REJECT_UNAUTHORIZED = 1;


/**
 *   Get MongoDB connection
 **/
var db = require('./config/dbconn.server.config')();


var port = process.env.PORT || 8082; // set our port

/**
 *   Set view path and view engine
 **/
app.set('views', __dirname + '/views');
app.set('view engine', 'pug');

/**
 *   Get all data/stuff of the body (POST) parameters
 **/
app.use(bodyParser.json()); // parse application/json 
app.use(bodyParser.json({type: 'application/vnd.api+json'})); // parse application/vnd.api+json as json
app.use(bodyParser.urlencoded({extended: true})); // parse application/x-www-form-urlencoded
app.use(require('cookie-parser')());
app.use(require('body-parser').urlencoded({extended: true}));
app.use(require('express-session')({
    secret: 'keyboard cat',
    resave: true,
    saveUninitialized: true
}));
app.use(passports.attach());
app.use(passports.middleware("initialize"));
app.use(passports.middleware("session"));
app.get('/login', passports.middleware("authenticate", 'oauth2'));

app.get('/auth/unifyd/passport/callback',
    passports.middleware("authenticate", "oauth2", {failureRedirect: '/login'}),
    function (req, res) {
        // Successful authentication, redirect home.
        res.redirect('/');
    });

app.use(methodOverride('X-HTTP-Method-Override')); // override with the X-HTTP-Method-Override header in the request. simulate DELETE/PUT
app.use(express.static(__dirname + '/public')); // set the static files location /public/img will be /img for users

/**
 *  Define routes for each product
 **/
require('./app/UniFyd/routes/admin.server.routes')(app);


/**
 *   Start the app
 **/
app.listen(port);
logger.info('Application started on port ' + port);

/**
 *  Return the global express app object
 **/
exports = module.exports = app; 					