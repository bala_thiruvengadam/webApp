angular.module('MainCtrl', []).controller('MainController', function ($scope, $rootScope, $http, blockUI) {

    

    /******************************DUMMY CODE FOR UI DEV ***********************/
    /***************************************************************************/
    /***************************************************************************/
    //$rootScope.user = {} // Remove this for production
    //$rootScope.user.accessToken = "Bearer a821e882-82d5-33ac-ad74-68852e1fa7ed";
    //$rootScope.user.tenant = "CEAI";
    //$rootScope.user.tenantdomain = "unifyd.com";


    $scope.image = "";
    $scope.userImg = 'http://mhalabs.org/wp-content/uploads/upme/1451456913_brodie.jpg' /* Dummy user image for devlopment */
    //$scope.$apply();

    $rootScope.GatewayUrl = "http://208.93.209.79/unifyd-gateway/api";
    //$rootScope.GatewayUrl = "http://gatewayAPIUrl"; /* Use this url for production env */

    $rootScope.callAPI = function(url,method,data,callback) {
        try{
        var apiEndPoint = $rootScope.GatewayUrl + url;
        console.log(apiEndPoint);
            var req = {
            method: 'POST',
            url: '/proxy',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json',
                'Authorization': 'Bearer '+$rootScope.user.accessToken,
                'X-TENANT-ID': $rootScope.user.tenant,
                'X-TENANT-DOMAIN': $rootScope.user.tenantdomain
            },
            data: {
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                    'Authorization': 'Bearer '+$rootScope.user.accessToken,
                    'X-TENANT-ID': $rootScope.user.tenant,
                    'X-TENANT-DOMAIN': $rootScope.user.tenantdomain
                },
                url: apiEndPoint,
                method: method,
                body: data,
                json: true
                }
            }
        console.log(req);

        $http(req).then(function successCallback(res) {
            console.log(res);
            callback(res);
        }, function errorCallback(err) {
            console.log(err);
        });
        }catch(e){console.log(e)}

    }

    var category = ['Official News','Athletics']; /* User preferances */
    var size = 10; /* number of records per call */
    var page = 0; /* intial page */
    var feedEndPoint = "/unifydwhatsup/feed";

    var feedUrl = feedEndPoint + "?size=" + size + "&category=" + category.toString() + "&sort=updatedDate,DESC&page=" + page; /* sort is fixed for api to be DESC*/

    $scope.loadPosts = function () {
        $rootScope.callAPI(feedUrl,'GET','', function(res) {
            //console.log(res.data);
            $scope.posts = res.data.content;
            $rootScope.posts = res.data.content;
        });
    }

    $http.get("/userinfo").then(function (resp) {
        console.log(resp.data);

        $rootScope.user = resp.data;

        $rootScope.user.userImg = "http://mhalabs.org/wp-content/uploads/upme/1451456913_brodie.jpg";
        $rootScope.user.userRole = "Admin";
        $scope.userImg = 'http://mhalabs.org/wp-content/uploads/upme/1451456913_brodie.jpg' /* Dummy user image for devlopment */
        
        $scope.loadPosts();
    }, function () {

    });

    $scope.textAreaAdjust = function(o) {
        o.style.height = "1px";
        o.style.height = (25 + o.scrollHeight) + "px";
    }


    $scope.createPost = function() {
        $location.path("/app/WhatsUp/WhatsUpPage2");
    }

    $scope.createPostWebApp =function(){
        $scope.showCreatePost = true;
    }

    $scope.deleteConfirm = function(btn) {
        if (btn == 1 || btn == true) {
            //$scope.posts.splice($scope.deleteId, 1);
            /* Add code for delete post api here */
            var postID = $scope.posts[$scope.deleteId].id;
            console.log(postID);

            var createPostUrl = "/unifydwhatsup/post/"+postID;

            $rootScope.callAPI(createPostUrl,'DELETE','', function(res) {
                console.log(res.data);
                $scope.loadPosts();
            });

        }
    }

    $scope.deletePost = function(index) {
        $scope.deleteId = index;
        if (window.device) {
            navigator.notification.confirm("Are you sure you want to delete this post?", $scope.deleteConfirm, 'Delete Post', ['Delete', 'No']);
        } else {
            //var res = confirm("Are you sure you want to delete this post?");
            //$scope.deleteConfirm(res);
            $.confirm({
                title: 'Delete Post',
                content: 'Are you sure you want to delete this post?',
                type:'red',
                buttons: {
                    confirm: {
                        btnClass: 'btn-red',
                        text: 'Delete',
                        action: function(){
                            $scope.deleteConfirm(1);
                        }
                    },
                    cancel: function () {

                    }
                }
            });
        }
    }

    var modal = document.getElementById('myModal');

    $scope.getPostLikes = function(e,index) {
        e.stopPropagation();
        $scope.showLikeModal = true;
        /* API code to get post likes */

        var getLikeUrl = "/unifydwhatsup/like/"+$scope.posts[index].id;
        $rootScope.callAPI(getLikeUrl,'GET','', function(res) {
            console.log(res.data);
            $scope.postLikes = res.data.content;
            $scope.totalLikes = res.data.content.length;
        });

        if(window.device){
            $("#myModal").show(500);
            $(".scrollable-content").css('overflow', 'hidden');
        }else{
            $("#myModal").show();
        }
    }

    $scope.getPostCommments = function(e,index) {
        //modal.style.display = 'block';
        e.stopPropagation();
        $scope.showLikeModal = false;

        var getCommentsUrl = "/unifydwhatsup/comment/"+$scope.posts[index].id;
        $scope.postCommentIndex = index;
        $rootScope.callAPI(getCommentsUrl,'GET','', function(res) {
                console.log(res.data);
            $scope.postComments = res.data.content;
            $scope.totalComments = res.data.content.length;
        });


        if(window.device){
            $("#myModal").show(500);
            $(".scrollable-content").css('overflow', 'hidden');
        }else{
            $("#myModal").show();
        }
    }

    $scope.postComment = function() {
        if ($scope.cmnt == null || $scope.cmnt == undefined || $scope.cmnt == "") {
            if(window.device) {
                navigator.notification.alert("Please add a comment", null, 'Alert', 'Ok');
            }else{
                //alert("Please add a comment");
                $.alert({
                    title:'Alert',
                    content:'Please add a comment',
                    backgroundDismiss: true
                });
            }
            return;
        } else {

            var commentdata = {
                "username": $rootScope.user.username,
                "userimage": $rootScope.user.userImg,
                "postId": $scope.posts[$scope.postCommentIndex].id,
                "comment" : $scope.cmnt
            }

            /* API to post comment */
            var postCommentUrl = "/unifydwhatsup/comment"
            $rootScope.callAPI(postCommentUrl,'POST',commentdata, function(res) {
                    console.log(res.data);
                    //$scope.loadPosts();
                if(res.data){
                    var commentdata = res.data;
                    $scope.postComments.splice(0, 0, commentdata);
                    $scope.cmnt = "";
                    $scope.posts[$scope.postCommentIndex].comment = $scope.posts[$scope.postCommentIndex].comment + 1;
                    $scope.totalComments = $scope.posts[$scope.postCommentIndex].comment;
                }else{

                }
            });

        }
    }

    $(".modal-contentTest").click(function(e) {
        e.stopPropagation();
    });


    $(document).click(function() {
        setTimeout(function(){
            $("#myModal").hide();
            $(".scrollable-content").css('overflow', 'auto');
        },100)
    });

    $scope.likePost = function(index) {
        $scope.posts[index].likes = $scope.posts[index].likes + 1;
        $scope.posts[index].liked = true;

        var createPostUrl = "/unifydwhatsup/like";
        var postData = {
            "username": $rootScope.user.username,
            "userimage": $rootScope.user.userImg,
            "postId": $scope.posts[index].id,
            "userrole" : $rootScope.user.userRole
        }
        $rootScope.callAPI(createPostUrl,'POST',postData, function(res) {
            console.log(res.data);
            //$scope.loadPosts();
        });
        $scope.$apply();
    }
    $scope.unlikePost = function(index) {
        $scope.posts[index].likes = $scope.posts[index].likes - 1;
        $scope.posts[index].liked = false;

        var createPostUrl = "/unifydwhatsup/like/"+$scope.posts[index].id;

        $rootScope.callAPI(createPostUrl,'DELETE','', function(res) {
            console.log(res.data);
            //$scope.loadPosts();
        });
        $scope.$apply();
    }

    $scope.$destroy = function() {
        //alert('des');
    }

    $("textarea").keyup(function() {

        if ($(this).val() !== '') {
            $(this).css('color', '#231b25');
        } else {
            $(this).css('color', '#a7b0c2');
        }

    });


    /*Create post */

    $scope.userImage = "http://mhalabs.org/wp-content/uploads/upme/1451456913_brodie.jpg";
    $scope.selectedtags = [];
    $scope.hideTaglist = true;

    $scope.Tags = [{
        id: 1,
        tag: 'Athletics',
        selected:false
    }, {
        id: 2,
        tag: 'Official News',
        selected:false
    }, {
        id: 3,
        tag: 'School News',
        selected:false
    }, {
        id: 4,
        tag: 'Placement',
        selected:false
    }, {
        id: 5,
        tag: 'Give To',
        selected:false
    }, {
        id: 6,
        tag: 'Workshop',
        selected:false
    }, {
        id: 7,
        tag: 'Greek Life',
        selected:false
    }, {
        id: 8,
        tag: 'Admission',
        selected:false
    }, {
        id: 9,
        tag: 'Official',
        selected:false
    }, {
        id: 10,
        tag: 'Continue Aid',
        selected:false
    }, {
        id: 11,
        tag: 'Academics',
        selected:false
    }, {
        id: 12,
        tag: 'Alumini',
        selected:false
    }, {
        id: 13,
        tag: 'Student Body',
        selected:false
    }, {
        id: 14,
        tag: 'Inquiry',
        selected:false
    }];

    $scope.addTags = function(tag) {
        for(var i=0;i<$scope.Tags.length;i++){
            if($scope.Tags[i].id == tag.id){
                $scope.Tags[i].selected = true;
            }
        }

        $scope.selectedtags.push(tag);
        $('#tagsList').hide();
    }

    $scope.removeTag = function(tag,index) {

        for(var i=0;i<$scope.Tags.length;i++){
            if($scope.Tags[i].id == tag.id){
                $scope.Tags[i].selected = false;
            }
        }

        for(var i=0;i<$scope.selectedtags.length;i++){
            if($scope.selectedtags[i].id == tag.id){
                $scope.selectedtags.splice(i,1)
            }
        }
        //document.getElementById("tag").focus();
    }

    $("#addTagsBtn").click(function(e) {
        $("#tagsList").show();
        document.getElementById("tagInput").focus();
        e.stopPropagation();
    });


    $("#tagsList").click(function(e) {
        e.stopPropagation();
    });

    $(document).click(function() {
        $("#tagsList").hide();
    });


    $scope.showUploadPhoto = true;

    $scope.SaveProfile = function() {

    }

    function onFail(message) {
        //alert('Failed because: ' + message);
        $.unblockUI();
    }

    $scope.cameraSuccess = function(imageData) {
        $scope.image = "data:image/png;base64," + imageData;
        $("#postImg").attr('src', $scope.image);
        $scope.showUploadPhoto = false;
        $scope.$apply();
        $.unblockUI();
    }

    $scope.useCamera = function() {
        $.blockUI({
            message: '<div id="floatingBarsG"><div class="blockG" id="rotateG_01"></div><div class="blockG" id="rotateG_02"></div><div class="blockG" id="rotateG_03"></div><div class="blockG" id="rotateG_04"></div><div class="blockG" id="rotateG_05"></div><div class="blockG" id="rotateG_06"></div><div class="blockG" id="rotateG_07"></div><div class="blockG" id="rotateG_08"></div></div><div></div>'
        });
        navigator.camera.getPicture($scope.cameraSuccess, onFail, {
            quality: 50,
            destinationType: Camera.DestinationType.DATA_URL
        });
    }

    $scope.useGallery = function() {
        $.blockUI({
            message: '<div id="floatingBarsG"><div class="blockG" id="rotateG_01"></div><div class="blockG" id="rotateG_02"></div><div class="blockG" id="rotateG_03"></div><div class="blockG" id="rotateG_04"></div><div class="blockG" id="rotateG_05"></div><div class="blockG" id="rotateG_06"></div><div class="blockG" id="rotateG_07"></div><div class="blockG" id="rotateG_08"></div></div><div></div>'
        });
        navigator.camera.getPicture($scope.cameraSuccess, onFail, {
            quality: 50,
            destinationType: Camera.DestinationType.DATA_URL,
            sourceType: Camera.PictureSourceType.PHOTOLIBRARY,
            popoverOptions: new CameraPopoverOptions(300, 300, 100, 100, Camera.PopoverArrowDirection.ARROW_ANY)
        });
    }

    var ActionSheetSuccess = function(btn) {
        if (btn == 1) {
            $scope.useCamera();
        } else {
            $scope.useGallery();
        }
    }

    $scope.openCamera = function() {
        var options = {
            'androidTheme': window.plugins.actionsheet.ANDROID_THEMES.THEME_HOLO_LIGHT,
            'title': '',
            'buttonLabels': ['Take a photo', 'Choose from gallery'],
            'addCancelButtonWithLabel': 'Cancel'
        };
        window.plugins.actionsheet.show(options, ActionSheetSuccess);
    }

    $scope.$destroy = function() {
        //$("#fixedFooterContent").html("");
    }


    var observe;
    if (window.attachEvent) {
        observe = function(element, event, handler) {
            element.attachEvent('on' + event, handler);
        };
    } else {
        observe = function(element, event, handler) {
            element.addEventListener(event, handler, false);
        };
    }

    function init() {
        var text = document.getElementById('postTitle');

        function resize() {
            text.style.height = 'auto';
            text.style.height = text.scrollHeight + 'px';
        }
        /* 0-timeout to get the already changed text */
        function delayedResize() {
            window.setTimeout(resize, 0);
        }
        observe(text, 'change', resize);
        observe(text, 'cut', delayedResize);
        observe(text, 'paste', delayedResize);
        observe(text, 'drop', delayedResize);
        observe(text, 'keydown', delayedResize);

        text.focus();
        text.select();
        resize();
    }
    init();


    $scope.removeImg = function() {
        $scope.image = "";
        $("#postImg").attr('src', "");
        $scope.showUploadPhoto = true;

        $scope.$apply();
    }

    $scope.addPost=function () {
        var tagsSelected = [];
        for(var i=0;i<$scope.selectedtags.length;i++){
            tagsSelected.push($scope.selectedtags[i].tag);
        }

        var postData = {
            "userName": $rootScope.user.username,
            /* dummy user for development */
            "userImage": $rootScope.user.userImg,
            "postTitle": $scope.postTitle,
            "postImage": $scope.img,
            "category": tagsSelected
        }
        //var postData = angular.toJson( postData );
        console.log(tagsSelected);
        console.log(postData);
        /* Add API to create post here, remove the dummy data below once integrated */
        var createPostUrl = "/unifydwhatsup/post";

        $rootScope.callAPI(createPostUrl,'POST',postData, function(res) {
            console.log(res.data);
            $scope.loadPosts();
        });

        //$rootScope.posts.splice(0, 0, newPostDummydata);
        $scope.showCreatePost = false;
        $scope.selectedtags = [];
        $scope.postTitle = "";
        $scope.image = "";
        $('#postImg').attr( "src", '');
        $scope.showUploadPhoto = true;
        for(var i=0;i<$scope.Tags.length;i++){
            $scope.Tags[i].selected = false;
        }
    }

    $scope.createPost = function() {
        if (($scope.postTitle == null || $scope.postTitle == undefined) && ($scope.image == "" || $scope.image == null || $scope.image == undefined)) {
            if(window.device){
                navigator.notification.alert("Please add title or an image to create a post.", null, 'Alert', 'Ok');
            }else{
                //alert("Please add title or an image to create a post.");
                $.alert({
                    title:'Alert',
                    content:'Please add title or an image to create a post.',
                    backgroundDismiss: true
                });
            }
            return;
        }

        if ($scope.selectedtags.length == 0) {
            if(window.device){
                navigator.notification.alert("Please add atleast one tag.", null, 'Alert', 'Ok');
            }else{
                //alert("Please add atleast one tag.");
                $.alert({
                    title:'Alert',
                    content:'Please add atleast one tag.',
                    backgroundDismiss: true
                });
            }
            return;
        }


        if ($scope.image != null && $scope.image != undefined && $scope.image != "") {
            var url = "https://kryptosda.kryptosmobile.com/kryptosds/api/imageUpload";
            var data = {
                'imageData': $('#postImg').attr("src").substring(22),
                "imageExtn": 'png'
            };

            $http.post(url, data).then(function(resp) {
                    $scope.img = resp.data.url;
                    $scope.addPost();
            });
        }else{
            $scope.addPost();
        }





        /* end of dummy data for create post */

        //$location.path("/app/WhatsUp/WhatsUp");

    }

    $("#postTitle").keyup(function() {

        if ($(this).val() !== '') {
            $(this).css('color', '#231b25');
        } else {
            $(this).css('color', '#a7b0c2');
        }

    });

    $scope.uploadImage = function(){
        $("#myPostImg").click();
    }

    $("#myPostImg").change(function () {
        if (this.files && this.files[0]) {
            var FR = new FileReader();
            FR.onload = function (e) {
                $scope.image = e.target.result;
                $scope.showUploadPhoto = false;
                $scope.showCreatePost = true;
                $('#postImg').attr( "src", $scope.image);
                $scope.$apply();
            };
            FR.readAsDataURL(this.files[0]);

            var img = new Image();
            var file = this.files[0];
            img.src = window.URL.createObjectURL(file);
        }
    });
    /* Create post ends */
    /***************************************************************************/
    /******************************DUMMY CODE FOR UI DEV ENDS***********************/
    /***************************************************************************/
});