angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function($routeProvider, $locationProvider) {

	$routeProvider

		// home page
		.when('/', {
			templateUrl: 'views/home',
			controller: 'MainController'
		})
		.when('/marketer', {
			templateUrl: 'views/marketer',
			controller: 'MarketerController'
		})
		.when('/nerds', {
			templateUrl: 'views/nerd',
			controller: 'NerdController'
		})
		.when('/logout', {
			templateUrl: 'views/home',
			controller: 'LogoutController'
		})
		.when('/geeks', {
			templateUrl: 'views/geek',
			controller: 'GeekController'	
		});

	$locationProvider.html5Mode(true);

}]);