/*******************************************************************************
*   © 2017 UniFyd.  All Rights Reserved.
*
*   CONFIDENTIAL BUSINESS INFORMATION
*   
*   This software includes proprietary, trade secret and confidential
*   information of UniFyd Organization and its licensors.  Use of 
*   this software is strictly subject to and governed by the terms
*   and conditions of a written software license agreement between
*   UniFyd Organization or its legal predecessor, and the
*   institution to which such license for use has been granted.
********************************************************************************/

var request        = require('request'),
   rp              = require('request-promise'),
   adminController = require('../controllers/admin.server.controller');


module.exports = function(app) {

  //app.get('/userinfo',             require('connect-ensure-login').ensureLoggedIn(), adminController.getUserInfo);

  //app.post('/proxy',               require('connect-ensure-login').ensureLoggedIn(), adminController.proxyApiCall);
      
  //app.get('/logout',               require('connect-ensure-login').ensureLoggedIn(), adminController.logout);

  //app.get('/views/:name', require('connect-ensure-login').ensureLoggedIn(), adminController.renderView);
  
  /**
  *   Route to handle all angular requests.
  **/
  //app.get('/',                     require('connect-ensure-login').ensureLoggedIn(), adminController.showHome);


  app.get('/userinfo', require('connect-ensure-login').ensureLoggedIn(), function(req, res) {
    console.log("* called..!!");
    console.log(req.user);
    res.json(req.user);
  });

app.post('/proxy', require('connect-ensure-login').ensureLoggedIn(), function(req, res) {

    console.log(req.body);

   var opt = req.body;

    request(opt, function(error, response, body) {
      console.log(error);
      console.log(response);
      console.log(body);
      res.send(body);
    });
    
});

app.get('/logout', require('connect-ensure-login').ensureLoggedIn(), function(req, res) {
    console.log("* called..!!");
    console.log(req.user);
    req.logout();
    //res.sendfile('./public/index.html');
    res.redirect("/");
  });

app.get('/views/:name', require('connect-ensure-login').ensureLoggedIn(), function(req, res) {
  var name = req.params.name;
    console.log("Called partials " + name);
    res.render('views/' + name);
});


  // frontend routes =========================================================
  // route to handle all angular requests
  app.get('/', require('connect-ensure-login').ensureLoggedIn(), function(req, res) {
    console.log("* called..!!");
    console.log(req.user);
    //res.sendfile('./public/index.html');
    res.render('index');
  });

};