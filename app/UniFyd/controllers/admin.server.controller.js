/*******************************************************************************
*   © 2017 UniFyd.  All Rights Reserved.
*
*   CONFIDENTIAL BUSINESS INFORMATION
*   
*   This software includes proprietary, trade secret and confidential
*   information of UniFyd Organization and its licensors.  Use of 
*   this software is strictly subject to and governed by the terms
*   and conditions of a written software license agreement between
*   UniFyd Organization or its legal predecessor, and the
*   institution to which such license for use has been granted.
********************************************************************************/
var request = require('request');
var rp = require('request-promise');
var logger = require('../../../config/logger.server.config');


exports.getUserInfo = function(req, res) {
      logger.debug("* called..!!");
      logger.debug(req.user);
      res.json(req.user);
	};

exports.proxyApiCall = function(req, res) {
    logger.debug(req.body);
    var opt = req.body;
	request(opt, function(error, response, body) {
	logger.debug(body);
	res.send(body);
	});      
  }

exports.logout = function(req, res) {
      logger.debug("* logout called..!!");
      logger.debug(req.user);
      req.logout();
      res.redirect("/");
    }

exports.renderView = function(req, res) {
  	var name    = req.params.name;
  	    //product = req.params.product;
    logger.debug("Called partials " + name);
    res.render('views/' + name);
}    

exports.showHome = function(req, res) {
  	logger.debug("* called..!!");
  	logger.debug(req.user);
  	//res.sendfile('./public/index.html');
  	res.render('index');
}